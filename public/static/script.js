(function (mapboxgl) {
    'use strict';

    var mapboxgl__default = 'default' in mapboxgl ? mapboxgl['default'] : mapboxgl;

    var MapboxControlPosition;
    (function (MapboxControlPosition) {
        MapboxControlPosition["TopLeft"] = "top-left";
        MapboxControlPosition["TopRight"] = "top-right";
        MapboxControlPosition["BottomLeft"] = "bottom-left";
        MapboxControlPosition["BottomRight"] = "bottom-right";
    })(MapboxControlPosition || (MapboxControlPosition = {}));
    function makeMapboxControl(control) {
        var fallbackContainer;
        if (typeof control.onAdd !== 'function') {
            control.onAdd = function () {
                fallbackContainer = document.createElement('div');
                fallbackContainer.setAttribute('class', 'mapboxgl-ctrl');
                return fallbackContainer;
            };
        }
        if (typeof control.onRemove !== 'function') {
            control.onRemove = function () {
                if (fallbackContainer instanceof HTMLElement &&
                    fallbackContainer.parentNode !== null) {
                    fallbackContainer.parentNode.removeChild(fallbackContainer);
                }
            };
        }
        if (typeof control.getDefaultPosition !== 'function') {
            control.getDefaultPosition = function () {
                return MapboxControlPosition.TopRight;
            };
        }
        return control;
    }

    var TestControl = (function () {
        function TestControl() {
            this.container = document.createElement('div');
            this.container.setAttribute('class', 'mapboxgl-ctrl mapboxgl-ctrl-group');
            var testButton = document.createElement('button');
            testButton.setAttribute('class', 'mapboxgl-ctrl-icon maps-af-mapbox-ctrl-test-button');
            testButton.addEventListener('click', function () { return alert('oh hai!!'); });
            this.container.appendChild(testButton);
        }
        TestControl.prototype.onAdd = function () {
            return this.container;
        };
        TestControl.prototype.onRemove = function () {
            if (this.container !== void 0 && this.container !== null && this.container.parentNode !== null) {
                this.container.parentNode.removeChild(this.container);
            }
        };
        return TestControl;
    }());

    var API_URL = 'https://api.mapbox.com';
    var ACCESS_TOKEN = 'pk.eyJ1IjoiZWRnYWxsaWdhbiIsImEiOiJZUzQ4dE9RIn0.TaaSiRg9r1wCtghbgQ0FOQ';
    var geocode = function (address, proximityBias) {
        var path = "/geocoding/v5/mapbox.places/" + encodeURIComponent(address) + ".json";
        var params = [
            "access_token=" + ACCESS_TOKEN,
            proximityBias !== undefined ? "proximity=" + proximityBias.join(',') : null,
        ];
        var url = "" + API_URL + path + "?" + params.join('&');
        console.log('Fetching', url);
        return fetch(url)
            .then(function (res) { return res.json(); })["catch"](function (e) { console.error(e); });
    };

    var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [0, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    };
    var SearchBoxControl = (function () {
        function SearchBoxControl() {
            var _this = this;
            this.proximityBias = [-6.260310, 53.349804];
            this.container = document.createElement('div');
            this.container.setAttribute('class', 'mapboxgl-ctrl mapboxgl-ctrl-group');
            var searchBox = document.createElement('input');
            searchBox.setAttribute('type', 'search');
            this.container.appendChild(searchBox);
            var searchButton = document.createElement('button');
            searchButton.setAttribute('class', 'mapboxgl-ctrl-icon maps-af-mapbox-ctrl-test-button');
            searchButton.addEventListener('click', function () { return __awaiter(_this, void 0, void 0, function () {
                var results;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, geocode(searchBox.value, this.proximityBias)];
                        case 1:
                            results = _a.sent();
                            console.log(results.features);
                            if (results !== undefined && results.features.length > 0) {
                                this.map.flyTo({
                                    center: results.features[0].geometry.coordinates,
                                    zoom: 16
                                });
                            }
                            debugOutput.textContent = JSON.stringify(results.features.map(function (feature) {
                                return feature.place_name;
                            }), null, '  ');
                            return [2];
                    }
                });
            }); });
            this.container.appendChild(searchButton);
            var debugOutput = document.createElement('pre');
            this.container.appendChild(debugOutput);
        }
        SearchBoxControl.prototype.onAdd = function (map) {
            this.map = map;
            return this.container;
        };
        SearchBoxControl.prototype.onRemove = function () {
            if (this.container !== void 0 && this.container !== null && this.container.parentNode !== null) {
                this.container.parentNode.removeChild(this.container);
            }
        };
        SearchBoxControl.prototype.getDefaultPosition = function () {
            return 'top-left';
        };
        return SearchBoxControl;
    }());

    mapboxgl__default.accessToken = 'pk.eyJ1IjoiZWRnYWxsaWdhbiIsImEiOiJZUzQ4dE9RIn0.TaaSiRg9r1wCtghbgQ0FOQ';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10'
    });
    map.addControl(makeMapboxControl(new TestControl()));
    map.addControl(makeMapboxControl(new SearchBoxControl()));
    map.addControl(new mapboxgl.ScaleControl());
    map.addControl(new mapboxgl.NavigationControl());
    map.addControl(new mapboxgl.GeolocateControl());
    map.addControl(new mapboxgl.FullscreenControl());

}(mapboxgl));
