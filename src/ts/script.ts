// vim: set et sw=4 ts=4 ff=unix ft=javascript :
/**
 * @project  : maps.af
 * @author   : code@edgalligan.com
 * @createdAt: 2018-03-23
 */


import mapboxgl, {
    Map,
    NavigationControl,
    ScaleControl,
    GeolocateControl,
    FullscreenControl,
} from 'mapbox-gl';
import { makeMapboxControl } from './maps.af/mapbox/makeMapboxControl';
import { TestControl } from './maps.af/controls/TestControl';
import { SearchBoxControl } from './maps.af/controls/SearchBoxControl';

mapboxgl.accessToken = 'pk.eyJ1IjoiZWRnYWxsaWdhbiIsImEiOiJZUzQ4dE9RIn0.TaaSiRg9r1wCtghbgQ0FOQ';
const map = new Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
});
map.addControl(makeMapboxControl(new TestControl()));
map.addControl(makeMapboxControl(new SearchBoxControl()));
map.addControl(new ScaleControl());
map.addControl(new NavigationControl());
map.addControl(new GeolocateControl());
map.addControl(new FullscreenControl());
