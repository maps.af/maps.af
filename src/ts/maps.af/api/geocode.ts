// vim: set et sw=4 ts=4 ff=unix ft=javascript :
/**
 * @project  : maps.af
 * @author   : code@edgalligan.com
 * @createdAt: 2018-08-21
 */

import { FeatureCollection, Position } from 'geojson';

const API_URL = 'https://api.mapbox.com';
const ACCESS_TOKEN = 'pk.eyJ1IjoiZWRnYWxsaWdhbiIsImEiOiJZUzQ4dE9RIn0.TaaSiRg9r1wCtghbgQ0FOQ';

export const geocode = (address: string, proximityBias?: Position): Promise<FeatureCollection | void> => {
    const path = `/geocoding/v5/mapbox.places/${encodeURIComponent(address)}.json`;
    const params = [
        `access_token=${ACCESS_TOKEN}`,
        proximityBias !== undefined ? `proximity=${proximityBias.join(',')}` : null,
    ];
    const url = `${API_URL}${path}?${params.join('&')}`;

    console.log('Fetching', url);

    return fetch(url)
    .then((res: Response): Promise<FeatureCollection> => res.json())
    .catch((e): void => { console.error(e); });
};

