// vim: set et sw=4 ts=4 ff=unix ft=javascript :
/**
 * @project  : maps.af
 * @author   : code@edgalligan.com
 * @createdAt: 2018-08-16
 */

import { IControl } from 'mapbox-gl';

enum MapboxControlPosition {
    TopLeft = 'top-left',
    TopRight = 'top-right',
    BottomLeft = 'bottom-left',
    BottomRight = 'bottom-right',
}

export function makeMapboxControl(control: any): IControl {

    let fallbackContainer: HTMLElement;

    if (typeof control.onAdd !== 'function') {
        control.onAdd = (): HTMLElement => {
            fallbackContainer = document.createElement('div');
            fallbackContainer.setAttribute('class', 'mapboxgl-ctrl');
            return fallbackContainer;
        };
    }

    if (typeof control.onRemove !== 'function') {
        control.onRemove = (): void => {
            // Since onRemove undoes onAdd's work, we check to see if we're
            // running our fallback onAdd from above and undo it's work here
            if (
                fallbackContainer instanceof HTMLElement &&
                fallbackContainer.parentNode !== null) {
                fallbackContainer.parentNode.removeChild(fallbackContainer);
            }
        }
    }

    if (typeof control.getDefaultPosition !== 'function') {
        control.getDefaultPosition = (): MapboxControlPosition => {
            return MapboxControlPosition.TopRight;
        }
    }

    return control;
}
