// vim: set et sw=4 ts=4 ff=unix ft=javascript :
/**
 * @project  : maps.af
 * @author   : code@edgalligan.com
 * @createdAt: 2018-08-21
 */

import { FeatureCollection } from 'geojson';
import { geocode } from '../api/geocode';

export class SearchBoxControl {
    container: HTMLElement;
    map: any;
    proximityBias = [-6.260310,53.349804];

    constructor() {
        this.container = document.createElement('div');
        this.container.setAttribute('class', 'mapboxgl-ctrl mapboxgl-ctrl-group');

        const searchBox = document.createElement('input');
        searchBox.setAttribute('type', 'search');
        this.container.appendChild(searchBox);

        const searchButton = document.createElement('button');
        searchButton.setAttribute('class', 'mapboxgl-ctrl-icon maps-af-mapbox-ctrl-test-button');
        searchButton.addEventListener('click', async () => {
            const results: FeatureCollection | void = await geocode(
                searchBox.value,
                this.proximityBias
            );

            console.log(results.features);
            if (results !== undefined && results!.features!.length > 0) {
                this.map.flyTo({
                    center: results.features[0].geometry.coordinates,
                    zoom: 16,
                });
            }

            debugOutput.textContent = JSON.stringify(results.features.map(feature => {
                return feature.place_name;
            }), null, '  ');
        });
        this.container.appendChild(searchButton);

        const debugOutput = document.createElement('pre');
        this.container.appendChild(debugOutput);
    }

    onAdd(map: any) {
        this.map = map;
        return this.container;
    }

    onRemove() {
        if (this.container !== void 0 && this.container !== null && this.container.parentNode !== null) {
            this.container.parentNode.removeChild(this.container);
        }
    }

    getDefaultPosition() {
        return 'top-left';
    }
}
