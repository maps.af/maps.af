// vim: set et sw=4 ts=4 ff=unix ft=javascript :
/**
 * @project  : maps.af
 * @author   : code@edgalligan.com
 * @createdAt: 2018-03-23
 */

export class TestControl {
    container: HTMLElement;

    constructor() {
        this.container = document.createElement('div');
        this.container.setAttribute('class', 'mapboxgl-ctrl mapboxgl-ctrl-group');

        const testButton = document.createElement('button');
        testButton.setAttribute('class', 'mapboxgl-ctrl-icon maps-af-mapbox-ctrl-test-button');
        testButton.addEventListener('click', () => alert('oh hai!!'));
        this.container.appendChild(testButton);
    }

    onAdd() {
        return this.container;
    }

    onRemove() {
        if (this.container !== void 0 && this.container !== null && this.container.parentNode !== null) {
            this.container.parentNode.removeChild(this.container);
        }
    }
}
